#pragma once

#include <typedefs.h>

#include "../../linalg/maths_3d.h"

namespace RED
{
    namespace input
    {
        enum class MOVE_DIR
        {
            FORWARD,
            BACKWARD,
            LEFT,
            RIGHT,
            AMT
        };

        struct MoveCommand
        {
            f32 amt = 0.;
        };

        struct RotateCameraCommand
        {
            math::v2 xy;
        };


        struct Input
        {
            MoveCommand moveCmds[(u32) MOVE_DIR::AMT];
            RotateCameraCommand rotCameraCmds;
            b32 stop_running;
        };

        math::v4 getMoveVec(Input input)
        {
            return 
            {
                input.moveCmds[(u32) MOVE_DIR::FORWARD].amt, 
                input.moveCmds[(u32) MOVE_DIR::BACKWARD].amt, 
                input.moveCmds[(u32) MOVE_DIR::LEFT].amt, 
                input.moveCmds[(u32) MOVE_DIR::RIGHT].amt
            };
        }
    }
}