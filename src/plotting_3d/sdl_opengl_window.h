#pragma once

#define GLEW_NO_GLU
#include <GL/glew.h>
#include <sdl2/SDL.h>
#include <sdl2/SDL_opengl.h>

#include "../typedefs.h"

#include "./opengl.h"

namespace RED
{
    namespace sdl_render
    {
        struct SDL_OpenGL_Window
        {
            SDL_Window *window;
            SDL_GLContext gl_context;         
            render::ogl::OpenGL gl;
        };

        void init()
        {
            SDL_Init(SDL_INIT_EVERYTHING);
        }

        SDL_OpenGL_Window generateSDLRenderWindow(f32 w, f32 h, const char *title)
        {
            SDL_Window *window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_OPENGL);
            SDL_GLContext gl_context = SDL_GL_CreateContext(window);
            assert(window);
            assert(gl_context);
            SDL_GL_MakeCurrent(window, gl_context);
            render::ogl::OpenGL gl = render::ogl::initContext(w, h);
            return { window, gl_context, gl };
        }
    }
}