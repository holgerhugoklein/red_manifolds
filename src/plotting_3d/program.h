#pragma once

#include <unordered_map>

#include "../typedefs.h"

namespace RED
{
    namespace render 
    {
        enum class SHADER_ATTR
        {
            A_POS,
            A_NORMAL,
            AMT
        };

        const char * attr_names[] = {
            "a_pos",
            "a_normal"
        };

        enum class UNIFORMS
        {
            VIEW,
            PROJECTION,
            MODEL_TRANSFORM,
            COLOR,
            POINT_LIGHT_1_POS,
            AMT
        } ;

        const char * unif_names[] = {
            "view",
            "projection",
            "model_transform",
            "color",
            "point_light_1_pos"
        };


        struct Program
        {
            u32 id;
            std::unordered_map<UNIFORMS, u32> unif_locations;
        };        
    }
}