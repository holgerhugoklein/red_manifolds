#pragma once

#include "shader/draw_points.h"

#include "util.h"
#include "program.h"

#include "primitives/curve.h"

namespace RED
{
    namespace render
    {
        namespace ogl
        {

            struct OpenGL
            {
                Program line_prog;
            };

            OpenGL initContext(f32 w, f32 h)
            {
                glewInit();
                glEnable(GL_DEPTH_TEST);
                glEnable(GL_BLEND);  
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  

                glViewport(0, 0, w, h);
                         
                glClearColor(0.99, 0.98, 0.98, 1.);   
                glPointSize(5);

                OpenGL res{};

                res.line_prog = util::loadShadersAndLinkProgramFromCode(
                    {
                        draw_points::VERT,
                        draw_points::FRAG
                    },
                    {
                        GL_VERTEX_SHADER,
                        GL_FRAGMENT_SHADER
                    }
                );

                return res;     
            }

            u32 pushVerticesOntoGPUReturnBuffer(math::v3 *verts, u32 amt)
            {
                u32 buffer;
                glGenBuffers(1, &buffer);
                glBindBuffer(GL_ARRAY_BUFFER, buffer);

                u32 size_in_bytes = amt * sizeof(math::v3);
                glBufferData(GL_ARRAY_BUFFER, size_in_bytes, verts, GL_DYNAMIC_DRAW);

                glEnableVertexAttribArray((u32) SHADER_ATTR::A_POS);
                glVertexAttribPointer((u32) SHADER_ATTR::A_POS, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

                return buffer;
            }

            void clear()
            {
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            }

            void renderLine(OpenGL *gl, math::m4 view, math::m4 proj, Curve c)
            {
                glUseProgram(gl->line_prog.id);

                glUniformMatrix4fv(gl->line_prog.unif_locations[UNIFORMS::VIEW], 1, GL_FALSE, glm::value_ptr(view));                
                glUniformMatrix4fv(gl->line_prog.unif_locations[UNIFORMS::PROJECTION], 1, GL_FALSE, glm::value_ptr(proj));     

                glBindBuffer(GL_ARRAY_BUFFER, c.ubo); 
                glEnableVertexAttribArray((u32) SHADER_ATTR::A_POS);
                glVertexAttribPointer((u32) SHADER_ATTR::A_POS, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
                
                glUniform3f(gl->line_prog.unif_locations[UNIFORMS::COLOR], c.color.r, c.color.g, c.color.b);             
                glDrawArrays(GL_LINE_STRIP, 0, c.points.size());
            }
        }
    }    
}