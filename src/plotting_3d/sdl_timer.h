#pragma once

#include <sdl2/SDL.h>

#include "../typedefs.h"

namespace RED
{
    namespace timer
    {
        struct SDL_Timer
        {
            u64 perf_freq_S;
            u32 start_ticks;
        };

        u64 ticks_now()
        {
            return SDL_GetPerformanceCounter();
        }

        f64 durationS(SDL_Timer timer, u64 start, u64 end)
        {
            return ((f64) end - (f64) start) / ((f64) timer.perf_freq_S);
        }
    }
}