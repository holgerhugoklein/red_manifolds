#pragma once

#include <vector>
#include <algorithm>

#include "./input/sdl_input.h"

#include "./sdl_opengl_window.h"
#include "./camera.h"
#include "./sdl_timer.h"

#include "primitives/curve.h"

namespace RED
{
    namespace plt
    {
        struct PlotWindow
        {
            sdl_render::SDL_OpenGL_Window sdl_ogl_window;   

            input::SDL_Input sdl_input;
            render::cam::Camera camera;
            f32 last_frame_durS;
            u64 frame_beginning_ticks;

            std::vector<render::Curve> curves;    
        };

        struct PLT
        {
            PLT()
            {
                sdl_render::init();
                plot_windows.reserve(124);
            }

            PlotWindow * generatePlotWindow(f32 w = 800, f32 h = 600, const char *title = "plot")
            {
                PlotWindow res{};
                res.sdl_input = input::setup();
                res.sdl_ogl_window = sdl_render::generateSDLRenderWindow(w, h, title);
                render::cam::setupCamera(&res.camera, math::v3(0, 0, 1), math::v3(0, 0, -1), w, h, glm::radians(45.f));

                plot_windows.push_back(res);
                
                return &plot_windows.back();
            }

            void plotCurve(PlotWindow * window, std::vector<math::v3> points, math::v3 color, u32 width)
            {
                SDL_GL_MakeCurrent(window->sdl_ogl_window.window, window->sdl_ogl_window.gl_context);
                u32 ubo = render::ogl::pushVerticesOntoGPUReturnBuffer(points.data(), points.size());
                window->curves.push_back({points, color, width, ubo});
            }

            void renderUntilUserClosesAll()
            {
                while(!plot_windows.empty())
                {
                    for(u32 i = 0; i < plot_windows.size();)
                    {
                        PlotWindow& plot_window = plot_windows[i];

                        f32 last_frame_durS = std::min(plot_window.last_frame_durS, 1.f / 30.f);
                        plot_window.frame_beginning_ticks = timer::ticks_now();
                        
                        input::Input input = input::generateFromSDL(&plot_window.sdl_input);
                        render::cam::move(&plot_window.camera, RED::input::getMoveVec(input), last_frame_durS);
                        render::cam::rotate(&plot_window.camera, input.rotCameraCmds.xy, last_frame_durS);     

                        SDL_GL_MakeCurrent(plot_window.sdl_ogl_window.window, plot_window.sdl_ogl_window.gl_context);
                        render::ogl::clear();

                        math::m4 view = render::cam::genLookAt(&plot_window.camera);
                        math::m4 proj = render::cam::genProjection(&plot_window.camera);                
                        
                        for(render::Curve c : plot_window.curves)
                        {
                            render::ogl::renderLine(&plot_window.sdl_ogl_window.gl, view, proj, c);
                        }

                        if(input.stop_running)
                        {
                            plot_windows.erase(plot_windows.begin() + i);
                        }
                        else
                        {
                            ++i;
                        }
                    }
                    for(u32 i = 0; i < plot_windows.size(); ++i)
                    {
                        PlotWindow& plot_window = plot_windows[i];
                        SDL_GL_MakeCurrent(plot_window.sdl_ogl_window.window, plot_window.sdl_ogl_window.gl_context);

                        SDL_GL_SwapWindow(plot_windows[i].sdl_ogl_window.window);
                        u64 frame_end_ticks = timer::ticks_now();
                        plot_window.last_frame_durS = timer::durationS(timer, plot_window.frame_beginning_ticks, frame_end_ticks);
                    }
                }
                
            }

            std::vector<PlotWindow> plot_windows;
            timer::SDL_Timer timer;
        };
    }
}