#pragma once

#include <vector>

#include "typedefs.h"
#include "linalg/maths_3d.h"

namespace RED
{
    namespace render
    {
        struct Curve
        {
            std::vector<math::v3> points;
            math::v3 color;
            u32 width;
            u32 ubo;
        };
    }
}