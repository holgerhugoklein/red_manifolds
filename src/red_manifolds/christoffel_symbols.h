#pragma once

#include "./matrix_types/red_linalg.h"

namespace RED
{
    namespace mfds
    {
        namespace christoffel_symbols
        {
            void calcChristoffelSymbols(math::Tensor_3d& out_res, math::Matrix G_inv, math::Tensor_3d dG)
            {
                u32 dim = G_inv.dimensions.x;

                for(u32 i = 0; i < dim; ++i)
                {
                    for(u32 j = 0; j < dim; ++j)
                    {
                        for(u32 k = 0; k < dim; ++k)
                        {
                            f32 chris_ijk = 0.f;
                            for(u32 l = 0; l < dim; ++l)
                            {
                                chris_ijk += G_inv[{k, l}] * (dG[{l, i, j}] + dG[{l, j, i}] - dG[{i, j, l}]);
                            }
                            chris_ijk *= .5;
                            out_res[{i, j, k}] = chris_ijk;
                        }
                    }   
                }
            }   
        }
    }
}