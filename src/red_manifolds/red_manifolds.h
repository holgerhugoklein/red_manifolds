#pragma once

#include "linalg/red_linalg.h"

#include "christoffel_symbols.h"

namespace RED
{
    namespace mfds
    {
        struct RED_Manifolds
        {
            RED_Manifolds(math::Linalg linalg) : linalg{linalg}
            {
            }

            math::Tensor_3d calcChristoffelSymbols(math::Matrix G_inv, math::Tensor_3d dG)
            {
                math::Tensor_3d res = linalg.createTensor3d(dG.dimensions);
                
                christoffel_symbols::calcChristoffelSymbols(res, G_inv, dG);

                return res;
            }

            math::Linalg linalg;
        };
    }
}