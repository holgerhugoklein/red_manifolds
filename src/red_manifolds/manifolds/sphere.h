#pragma once

#include <math.h>

#include "typedefs.h"

#include "linalg/maths_3d.h"
#include "linalg/matrix_types/matrix.h"
#include "linalg/matrix_types/tensor.h"

namespace RED
{
    namespace mfds
    {
        //taken from https://www.cis.upenn.edu/~cis610/cis610-18-sl11.pdf
        namespace sphere
        {
            math::v3 embeddIn3d(f32 colat, f32 lon)
            {
                f32 x = sin(colat) * cos(lon);
                f32 y = sin(colat) * sin(lon);
                f32 z = cos(colat); 

                return {x, y, z}; 
            }
            
            void calcG(math::Matrix& res, f32 colat, f32 lon)
            {
                assert(res.dimensions.x == 2 && res.dimensions.y == 2);

                res[{0, 0}] = 1.;
                res[{1, 0}] = 0.;
                res[{0, 1}] = 0.;
                res[{1, 1}] = sin(colat) * sin(colat);
            }

            void calcdG(math::Tensor_3d& res, f32 colat, f32 lon)
            {
                assert(res.dimensions.x == 2 && res.dimensions.y == 2 && res.dimensions.z == 2);
                res.clear();
                res[{1, 1, 0}] = -2 * sin(colat) * cos(colat);
            }
        }       
    }
}