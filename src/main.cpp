#include <stdio.h>

#include "plotting_3d/plot.h"

#include "linalg/maths_3d.h"
#include "linalg/red_linalg.h"

#include "linalg/matrix_types/tensor.h"

#include "red_manifolds/manifolds/sphere.h"


void pushSphereCurves(RED::plt::PLT *plt, RED::plt::PlotWindow *pw)
{
    u32 amt_steps = 50;
    f32 colat_size = 2 * M_PI / (amt_steps + 1);
    f32 lon_size = colat_size;

    for(f32 colat = 0.; colat <= 2 * M_PI; colat += colat_size)
    {
        std::vector<RED::math::v3> points;
        points.reserve(amt_steps);
        for(f32 lon = 0.; lon <= 2 * M_PI; lon += lon_size)
        {
            RED::math::v3 r3 = RED::mfds::sphere::embeddIn3d(colat, lon);            
            points.push_back(r3);
        }
        plt->plotCurve(pw, points, {1, 0, 0}, 5);
    }

    for(f32 lon = 0.; lon <= 2 * M_PI; lon += lon_size)
    {
        std::vector<RED::math::v3> points;
        points.reserve(amt_steps);
        for(f32 colat = 0.; colat <= 2 * M_PI; colat += colat_size)
        {
            RED::math::v3 r3 = RED::mfds::sphere::embeddIn3d(colat, lon);            
            points.push_back(r3);
        }
        plt->plotCurve(pw, points, {1, 0, 0}, 5);
    }
}

int main(int argc, char **argv)
{
    f32 temp[3 * 3];
    RED::math::Matrix m{{3, 3}, temp};
    m.clear();

    m.setRow(0, {25, 15, -5});
    m.setRow(1, {15, 18, 0});
    m.setRow(2, {-5, 0, 11});

    m.print();

    RED::math::Linalg linalg{};

    linalg.invertPosDefSymm(m);

    RED::plt::PLT plt{};
    RED::plt::PlotWindow *pw = plt.generatePlotWindow();

    pushSphereCurves(&plt, pw);
        
    plt.renderUntilUserClosesAll();

    f32 data[3 * 3 * 3];
    RED::math::Tensor_3d tensor{3, 3, 3, data};
    tensor.clear();
    tensor[{1, 0, 0}] = 1.;
    tensor.print();
    return 0;
}