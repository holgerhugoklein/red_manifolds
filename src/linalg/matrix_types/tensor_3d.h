#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../../typedefs.h"

namespace RED
{
    namespace math
    {
        struct CoordTriple
        {
            u32 x;
            u32 y;
            u32 z;
        };

        struct Tensor_3d
        {
            Tensor_3d(u32 dim_x, u32 dim_y, u32 dim_z, f32 *data) : dimensions{dim_x, dim_y, dim_z}, data{data}
            {
            }

            Tensor_3d(CoordTriple dims, f32 *data) : dimensions{dims}, data{data}
            {
            }

            u32 calcArrayIndex(CoordTriple coords) const
            {
                return coords.z * (dimensions.x * dimensions.y) + coords.y * dimensions.x + coords.x;
            }

            f32& operator[](CoordTriple coords)
            {
                u32 idx = calcArrayIndex(coords);
                return data[idx];
            }

            void clear()
            {
                memset(data, 0, dimensions.x * dimensions.y * dimensions.z * sizeof(f32));
            }

            void print()
            {
                for(u32 z = 0; z < dimensions.z; ++z)
                {
                    for(u32 y = 0; y < dimensions.y; ++y)
                    {
                        for(u32 x = 0; x < dimensions.x; ++x)
                        {
                            printf("%f, ", data[calcArrayIndex({x, y, z})]);
                        }
                        printf("\n");
                    }
                    printf("\n");
                }
            }

            CoordTriple dimensions;
            f32 *data;
        };   
    }
}