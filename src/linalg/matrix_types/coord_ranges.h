#pragma once

#include "typedefs.h"

namespace RED
{
    namespace math
    {
          enum class CoordRangeTypes
        {
            ONE,
            RANGE,
            ALL
        };

        struct CoordRangeOne
        {
            CoordRangeTypes type;
            u32 n1;

            CoordRangeOne(u32 n1) : n1{n1}, type{CoordRangeTypes::ONE} {}
        };

        
        struct CoordRangeRange
        {
            CoordRangeTypes type = CoordRangeTypes::RANGE;
            u32 n1;
            u32 n2;
            CoordRangeRange(u32 n1, u32 n2) : n1{n1}, n2{n2}, type{CoordRangeTypes::RANGE} {}
        };

        struct CoordRangeAll
        {
            CoordRangeTypes type;
            CoordRangeAll() : type{CoordRangeTypes::ALL} {}
        };        

        struct CoordRangeCombined
        {
            union 
            {
                CoordRangeOne one;
                CoordRangeRange range;
                CoordRangeAll all;
            };            
        };
    }
}