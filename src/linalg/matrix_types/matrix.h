#pragma once

#include <string.h>
#include <initializer_list>

#include "typedefs.h"

#include "coord_ranges.h"

namespace RED
{
    namespace math
    {

        struct CoordTuple
        {
            u32 x;
            u32 y;
        };

        struct Matrix
        {
            Matrix(CoordTuple dimensions, f32 *data) : dimensions{dimensions}, data{data}
            {
            }

            void clear()
            {
                memset(data, 0, dimensions.x * dimensions.y * sizeof(f32));
            }

            u32 calcArrayIndex(CoordTuple coords)
            {
                return coords.y * dimensions.x + coords.x;
            }

            f32& operator[](CoordTuple coords)
            {
                u32 idx = calcArrayIndex(coords);
                return data[idx];
            }

            void setRow(u32 row, std::initializer_list<f32> ns)
            {
                assert(ns.size() == dimensions.x);
                u32 idx = calcArrayIndex({0, row});
                memcpy(data + idx, ns.begin(), ns.size() * sizeof(f32));
            }

            void print()
            {
                for(u32 y = 0; y < dimensions.y; ++y)
                {
                    for(u32 x = 0; x < dimensions.x; ++x)
                    {
                        printf("%f, ", data[calcArrayIndex({x, y})]);
                    }
                    printf("\n");
                }
            }
            
            CoordTuple dimensions;
            f32 *data;
        };
    }
}