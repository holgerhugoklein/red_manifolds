#pragma once

#include <initializer_list>
#include <string.h>

#include "typedefs.h"

#include "coord_ranges.h"

namespace RED
{
    namespace math
    {
        const u32 MAX_DIM = 8;
        struct Coords
        {
            u32 c[MAX_DIM];

            void clear()
            {
                memset(c, 0, sizeof(c));
            }
        };

        struct Tensor
        {
            Tensor(u32 dim, f32 *data, Coords shape) : dim{dim}, shape{shape}, data{data}
            {
                assert(dim < MAX_DIM);
                stride.clear();
                offset.clear();
                offset_index = 0;
            }

            Tensor(u32 dim, f32 *data, Coords shape, Coords stride, Coords offset) : 
                dim{dim}, shape{shape}, data{data}, stride{stride}, offset{offset}
            {
                offset_index = offToIndex();
            }

            Tensor operator[](std::initializer_list<CoordRangeCombined> ranges)
            {
                assert(ranges.size() == dim);
                for(auto r : ranges)
                {

                }
            }

            u32 offToIndex()
            {
                u32 idx = 0;
                for(u32 i = dim - 1; i > 0; --i)
                {
                    u32 amt_off = offset.c[i];
                    u32 offset_multiply = 1;
                    for(u32 j = 0; j < i; ++j)
                    {
                        offset_multiply *= shape.c[j];
                    }
                    idx += amt_off * offset_multiply;
                }
                return idx;
            }

            u32 getIdx(Coords c)
            {
                for(u32 i = 0; i < dim; ++i)
                {
                    
                }
            }

            f32& operator[](Coords c)
            {

            }

            void print()
            {
                Coords coords{};
                coords.clear();


            }

            u32 dim;
            u32 offset_index;
            Coords shape;
            Coords stride;
            Coords offset;
            f32 *data;
        };
    }
}