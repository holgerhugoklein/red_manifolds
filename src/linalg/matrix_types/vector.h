#pragma once
#include "../../typedefs.h"

namespace RED
{
    namespace math
    {
        struct Vector
        {
            u32 dim;
            f32 *data;
        };        
    }
}