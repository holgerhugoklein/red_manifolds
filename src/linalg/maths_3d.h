#pragma once

#include <string>

#define GLM_FORCE_PURE 
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../typedefs.h"

namespace RED
{
    namespace math
    {
        typedef glm::vec2 v2;
        typedef glm::vec3 v3;
        typedef glm::vec4 v4;
        
        typedef glm::uvec2 uv2;
        typedef glm::uvec3 uv3;
        typedef glm::uvec4 uv4;
        
        typedef glm::ivec2 iv2;
        typedef glm::ivec3 iv3;
        typedef glm::ivec4 iv4;
        
        typedef glm::mat2 m2;
        typedef glm::mat3 m3;
        typedef glm::mat4 m4;

        typedef glm::quat quat;

        struct Ray
        {
            v3 origin;
            v3 dir;
        };

        struct Rect
        {               
            v3 color;
            v3 min;
            v3 max;
        };

        struct Point
        {
            v3 color;
            v3 pos;
        };

        struct Triangle
        {
            v3 color;
            v3 p1;
            v3 p2;
            v3 p3;
        };

        void calcPrefixSums(u32 *in_out_data, u32 size)
        {
            u32 current_sum = 0;
            for(u32 i = 0; i < size; ++i)
            {
                current_sum += in_out_data[i];
                in_out_data[i] = current_sum;
            }
        }

        v3 toV3(quat q)
        {
            return {q.x, q.y, q.z};
        }

        v3 applyQuaternionToV3(quat q, v3 vertex)
        {
            quat rot = q * quat(0., vertex.x, vertex.y, vertex.z) * glm::inverse(q);
            assert(rot.w == 0.);
            v3 res = toV3(rot);
            return res;
        }
        
        math::v3 applyTransform(math::m4 mat, math::v3 vertex)
        {
            math::v4 transformed = mat * math::v4{vertex, 1.};
            math::v3 res = math::v3{transformed};
            return res;
        }

        std::string toStr(math::v3 v3)
        {
            return {"(" + std::to_string(v3.x) + ","+ std::to_string(v3.y) + ","+ std::to_string(v3.z) + ")\n"};
        }

        math::quat quatFromAngularVel(math::v3 angular_vel)
        {
            return math::quat(0., angular_vel);
        }
    }
}