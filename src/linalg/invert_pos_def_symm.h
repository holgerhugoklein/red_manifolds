#pragma once

#include <assert.h>
#include <math.h>

#include "typedefs.h"

#include "matrix_types/matrix.h"


namespace RED
{
    namespace math
    {        
        //find the inverse of a positive definite symmetric matrix by first
        //calculating its cholesky decomposition
        void invertPositiveDefiniteSymmetricMatrix(Matrix& mat, Matrix& res)
        {
            assert(mat.dimensions.x == mat.dimensions.y);
            assert(res.dimensions.x == res.dimensions.y);
            assert(res.dimensions.x == mat.dimensions.x);
            
            res.clear();

            //calculate the cholesky decomposition
            for(u32 y = 0; y < mat.dimensions.y; ++y)
            {
                for(u32 x = 0; x < mat.dimensions.x; ++x)
                {
                    if(x == y)
                    {
                        f32 sum_to_subtract = 0.;
                        for(u32 k = 0; k < x; ++k)
                        {
                            sum_to_subtract += res[{x, k}] * res[{x, k}]; 
                        }
                        res[{x, y}] = sqrt(mat[{x, y}] - sum_to_subtract);
                    }
                    else
                    {
                        f32 sum_to_subtract = 0.;
                        for(u32 k = 0; k < x; ++k)
                        {
                            sum_to_subtract += res[{k, y}] * res[{k, x}]; 
                        }
                        res[{x, y}] = (1 / res[{x, x}]) * (mat[{x, y}] - sum_to_subtract);
                    }
                }
            }
            res.print();
        }
    }
}