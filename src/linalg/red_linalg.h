#pragma once

#include <assert.h>

#include "matrix_types/matrix.h"
#include "matrix_types/tensor_3d.h"
#include "matrix_types/vector.h"

#include "invert_pos_def_symm.h"


namespace RED
{
    namespace math
    {
        struct F32Memory
        {
            
            F32Memory(u32 cap, f32 *data) : cap{cap}, size{0}, data{data}
            {                
            }

            F32Memory(u32 cap) : cap{cap}, size{0}
            {
                data = (f32 *) malloc(cap * sizeof(f32));                
            }

            f32 *alloc(u32 amt_floats)
            {
                assert(size + amt_floats < cap);
                f32 *res = data + size;
                size += amt_floats;
                return res;
            }

            u32 cap;
            u32 size;
            f32 *data;
        };


        struct Linalg
        {   
            Linalg(F32Memory memory) : memory{memory}
            {
            }

            Linalg(u32 cap) : memory{cap}
            {
            }

            Linalg() : memory{1024 * 8}
            {                
            }

            Tensor_3d createTensor3d(CoordTriple dimensions)
            {
                f32 *data = memory.alloc(dimensions.x * dimensions.y * dimensions.z);
                Tensor_3d res{dimensions, data};
                return res;
            }

            Matrix invertPosDefSymm(Matrix& mat)
            {
                f32 *data = memory.alloc(mat.dimensions.x * mat.dimensions.y);
                Matrix res{mat.dimensions, data};
                invertPositiveDefiniteSymmetricMatrix(mat, res);
                return res;
            }



            F32Memory memory;            
        };
    }
}